import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import TokenService from '@/services/token.service'

Vue.use(VueRouter)

const requireAuth = function (to, from, next) {
  if (!TokenService.hasToken()) {
      next({ name: 'signin', query: { redirect: to.fullPath }});
  } else {
      next();
  }
}


const requireGuest = function (to, from, next) {
  if (TokenService.hasToken()) {
      next({ name: 'dashboard' });
  } else {
      next()
  }
}

const logout = function (to, from, next) {
  TokenService.removeToken()
  store.dispatch('logout')
  next({ name: 'signin'})
}


const routes = [
  { path: '/logout', name: 'logout', beforeEnter: logout },
  { path: '*',  redirect: '/' },
  { path: '/redirect-me',  redirect: { name: 'home' } },
  { path: '/', name: 'home', component: () => import('../views/Home.vue') },
  { path: '/lifecycle', name: 'lifecycle', component: () => import('../views/Lifecycles.vue') },
  { path: '/inputs', name: 'inputs', component: () => import('../views/Inputs.vue') },
  { path: '/mixins', name: 'mixins', component: () => import('../views/MixinsFilters.vue') },
  { path: '/animations', name: 'animations', component: () => import('../views/Animations.vue') },
  { path: '/statemanagement', name: 'statemanagement', component: () => import('../views/StateManagement.vue') },
  { path: '/axioshttp',
    component: () => import('@/views/AxiosHTTP.vue'),
    children: [
      { path: '', name: 'welcome', component: () => import('@/components/auth/user/welcome/welcome.vue'), beforeEnter: requireGuest },
      { path: 'signin', name: 'signin', component: () => import('@/components/auth/signin.vue'), beforeEnter: requireGuest },
      {
        path: 'dashboard',
        name: 'dashboard',
        component: () => import('@/components/auth/user/dashboard/dashboard.vue'),
        beforeEnter: requireAuth
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    if (to.hash) {
      return { selector: to.hash };
    }
  }
})

router.beforeEach((to, from, next) => {
  console.log('global before each router')
  next();
}) 

export default router
