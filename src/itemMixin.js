export const itemMixin = {
    data () {
        return {
            items: ['Apple', 'Banana', 'Mango', 'Melon'],
            filterText: ''
        }
    },
    computed: {
        filteredItems () {
            return this.items.filter((element) => {
                return element.match(this.filterText);
            })
        }
    },
    created () {
        console.log('created')
    }
}