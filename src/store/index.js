import Vue from 'vue'
import Vuex from 'vuex'
import Counter from './modules/counter'
import Auth from './modules/auth'
import * as actions from './modules/value/actions'
import * as mutations from './modules/value/mutations'
import * as getters from './modules/value/getters'

Vue.use(Vuex)

const Value = {
  namespaced: true,
  state: {
    value: 0
  },
  actions,
  mutations,
  getters,
}

export default new Vuex.Store({
  modules: {
    Value,
    Counter,
    Auth
  }
})
