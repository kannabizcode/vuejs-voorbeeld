import * as actions from './value/actions'
import * as mutations from './value/mutations'
import * as getters from './value/getters'


export const value = {
  actions,
  mutations,
  getters,
}
