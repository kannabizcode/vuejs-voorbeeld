import AuthService from '@/services/auth.service'
import axios from 'axios'
import router from '../../router'

const state = {
  idToken: null,
  userId: null,
  user: null,
}

const getters = {
  user(state) {
    return state.user
  },
  isAuthenticated(state) {
    return state.idToken !== null;
  }
}

const mutations = {
  authUser(state, userData) {
    state.idToken = userData.token;
    state.userId = userData.userId;
  },
  storeUser(state, user) {
    state.user = user;
  },
  clearAuthData(state) {
    state.idToken = null
    state.userId = null
  }
}

const actions = {
  setLogoutTimer({
    commit
  }, expirationTime) {
    setTimeout(() => {
      commit('clearAuthData')
    }, expirationTime * 1000)
  },
  login({
    commit,
    dispatch
  }, authData) {
    AuthService.post('/accounts:signInWithPassword?key=AIzaSyAkthnU_-FpVy-Nha1m94x2Fnnw3hnHr_8', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
      .then(res => {
        commit('authUser', {
          token: res.data.idToken,
          userId: res.data.localId
        })
        const now = new Date()
        const expirationDate = new Date(now.getTime() + res.data.expiresIn * 1000);
        localStorage.setItem('idToken', res.data.idToken);
        localStorage.setItem('userId', res.data.localId);
        localStorage.setItem('expirationDate', expirationDate);
        dispatch('setLogoutTimer', res.data.expiresIn)
        router.replace({
          name: 'dashboard'
        })
      })
      .catch(error => console.log(error))
  },
  tryAutoLogin({
    commit
  }) {
    const token = localStorage.getItem('idToken')
    if (!token) {
      return;
    }
    const expirationDate = localStorage.getItem('expirationDate')
    const now = new Date()
    if (now >= expirationDate) {
      return;
    }
    const userId = localStorage.getItem('userId')
    commit('authUser', {
      token: token,
      userId: userId
    })
  },
  logout({
    commit
  }) {
    commit('clearAuthData')
    localStorage.removeItem('idToken');
    localStorage.removeItem('userId');
    localStorage.removeItem('expirationDate');
  },
  storeUser({
    commit,
    state
  }, userData) {
    if (!state.idToken) {
      return;
    }
    axios.post('/users.json' + '?auth=' + state.idToken, userData).then(res => {
      console.log(res)
    }).catch(error => {
      console.log(error)
    })
  },
  fetchUser({
    commit,
    state
  }) {
    if (!state.idToken) {
      return;
    }
    axios.get('/users.json' + '?auth=' + state.idToken)
      .then(res => {
        const data = res.data
        const users = []
        for (let key in data) {
          const user = data[key]
          user.id = key;
          users.push(user)
        }
        commit('storeUser', users[0])
      })
      .catch(error => console.log(error))
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}