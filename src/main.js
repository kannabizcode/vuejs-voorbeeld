import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '../node_modules/animate.css'
import VueResource from 'vue-resource'
import axios from 'axios'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

axios.defaults.baseURL = 'https://vuejs-fab81.firebaseio.com';

const requestInterceptors = axios.interceptors.request.use((config) => {
  console.log(`Request interceptor: `, config)
  return config;
})

const responseInterceptors = axios.interceptors.response.use((res) => {
  console.log(`Response interceptors: `, res)
  return res;
})

axios.interceptors.request.eject(requestInterceptors);
axios.interceptors.response.eject(responseInterceptors);

Vue.config.productionTip = false

Vue.directive('highlight', {
  bind (el, binding, vnode) {
    let delay = 0;

    if (binding.modifiers['delayed']) {
      delay = 3000;
    }

    setTimeout(() => {
      if (binding.arg == 'background') {
        el.style.backgroundColor = binding.value
      } 
  
      if (binding.arg == 'color') {
        el.style.color = binding.value
      } 
    }, delay)
  }
});

// global filters
Vue.filter('to-lowercase', function (value) {
  return value.toLowerCase();
})

export const eventBus = new Vue({
  methods: {
    // Custom Global Events
    resetServer (value) {
      this.$emit('serverReset', value)
    }
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
